package ch.cern.todo.dto;

public class CategoryDto {
    private String name;
    private String description;

    public CategoryDto(String name) {
        this.name = name;
    }

    public CategoryDto(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
