package ch.cern.todo.dto;

import java.time.LocalDateTime;

public class TaskDto {
    private String name;
    private String description;
    private LocalDateTime deadline;
    private Long category_id;

    public TaskDto(String name, String description, LocalDateTime deadline, Long category_id) {
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.category_id = category_id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
    }

    public LocalDateTime getDeadline() {
        return deadline;
    }

    public void setCategory_id(Long category_id) {
        this.category_id = category_id;
    }

    public Long getCategory_id() {
        return category_id;
    }
}
