package ch.cern.todo.service;

import ch.cern.todo.dto.TaskDto;
import ch.cern.todo.model.Category;
import ch.cern.todo.model.Task;
import ch.cern.todo.repository.CategoryRepository;
import ch.cern.todo.repository.TaskRepository;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class TaskService {
    private final TaskRepository taskRepository;
    private final CategoryRepository categoryRepository;

    public TaskService(TaskRepository taskRepository, CategoryRepository categoryRepository) {
        this.taskRepository = taskRepository;
        this.categoryRepository = categoryRepository;
    }

    public Task addTask(TaskDto newTaskDto) {
        Task newTask = new Task(
                newTaskDto.getName(),
                newTaskDto.getDeadline(),
                newTaskDto.getDescription()
        );

        Optional<Category> category = categoryRepository.findById(newTaskDto.getCategory_id());

        if (category.isEmpty()) {
            throw new NoSuchElementException("Category does not exist");
        }

        newTask.setCategory(category.get());

        return taskRepository.save(newTask);
    }

    public void updateTask(Long id, TaskDto newTaskDto) {
        Optional<Task> existingTaskData = taskRepository.findById(id);

        if (existingTaskData.isEmpty()) {
            throw new NoSuchElementException("Task does not exist");
        }

        Task existingTask = existingTaskData.get();

        if (newTaskDto.getName() != null) {
            existingTask.setName(newTaskDto.getName());
        }

        if (newTaskDto.getDescription() != null) {
            existingTask.setDescription(newTaskDto.getDescription());
        }

        if (newTaskDto.getDeadline() != null) {
            existingTask.setDeadline(newTaskDto.getDeadline());
        }

        if (newTaskDto.getCategory_id() != null) {
            Optional<Category> categoryData = categoryRepository.findById(newTaskDto.getCategory_id());

            if (categoryData.isEmpty()) {
                throw new NoSuchElementException("Category does not exist");
            }

            Category category = categoryData.get();
            existingTask.setCategory(category);

        }
    }
}
