package ch.cern.todo.service;

import ch.cern.todo.dto.CategoryDto;
import ch.cern.todo.model.Category;
import ch.cern.todo.repository.CategoryRepository;

import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category addCategory(CategoryDto newCategoryDto) {
        Category newCategory = new Category(
                newCategoryDto.getName(),
                newCategoryDto.getDescription()
        );

        return categoryRepository.save(newCategory);
    }

    public void updateCategory(Long id, CategoryDto newCategory) {
        Optional<Category> existingCategoryData = categoryRepository.findById(id);

        if(existingCategoryData.isEmpty()) {
            throw new NoSuchElementException("Category does not exist");
        }

        Category existingCategory = existingCategoryData.get();

        if (newCategory.getName() != null) {
            existingCategory.setName(newCategory.getName());
        }

        if (newCategory.getDescription() != null) {
            existingCategory.setDescription(newCategory.getDescription());
        }

        categoryRepository.save(existingCategory);
    }
}
