package ch.cern.todo.exception;

import java.time.LocalDateTime;
import java.util.List;

public class ErrorResponse {
    private int statusCode;
    private String message;
    private LocalDateTime timestamp;
    private String exceptionType;

    public ErrorResponse(int statusCode, String message, LocalDateTime timestamp, String exceptionType) {
        this.statusCode = statusCode;
        this.message = message;
        this.timestamp = timestamp;
        this.exceptionType = exceptionType;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setExceptionType(String exceptionType) {
        this.exceptionType = exceptionType;
    }

    public String getExceptionType() {
        return exceptionType;
    }
}
